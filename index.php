<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercice parametres php</title>
</head>
<body>
 

<h1>IV Les paramètres</h1>

<h1>Exercice 1 </h1>
<!-- Faire une page index.php. 
Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
index.php?nom=Nemare&prenom=Jean -->


<a href="index.php?nom=Nemare&amp;prenom=Jean">Dis-moi bonjour !</a>

<p>Bonjour <?php echo $_GET['prenom'] . ' ' . $_GET['nom']; ?> !</p>

<h1>Exercice 2 </h1>
<!-- Faire une page index.php. 
Tester sur cette page que le paramètre age existe et si c'est le cas l'afficher sinon le signaler
 :_ index.php?nom=Nemare&prenom=Jean_ -->

 <?php


if (isset($_GET['prenom']) && isset($_GET['nom'])) // On a le nom et le prénom
{
	echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !';
}
else // Il manque des paramètres, on avertit le visiteur
{
	echo 'Il faut renseigner un nom et un prénom !';
}
?>

<h1>**Exercice 3 **</h1>
<!-- Faire une page index.php. 
Tester sur cette page que tous les paramètres de cette URL existent et les afficher:
 index.php?dateDebut=2/05/2016&dateFin=27/11/2016 -->

 <a href="index.php?dateDebut=2/05/2016&dateFin=27/11/2016"> C'est ici</a>
 <?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

 echo ($_GET["dateDebut"]);
 echo ($_GET["dateFin"]);
 ?>

 
<h1>Exercice 4 </h1>
<!-- Faire une page index.php. 
Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
index.php?langage=PHP&serveur=LAMP -->

<a href="index.php?langage=PHP&serveur=LAMP">Erreur </a>
<?php

echo ($_GET["langage"]);
echo ($_GET["serveur"]);

?>


<h1>Exercice 5 </h1>
<!-- Faire une page index.php. 
Tester sur cette page que tous les paramètres de cette URL existent et les afficher:
 index.php?semaine=12 -->

 <a href="index.php?semaine=12 "> Ici</a>
 <?php
 
 echo ($_GET["semaine"]);


 ?>


<h1>**Exercice 6 **</h1>
<!-- Faire une page index.php.
 Tester sur cette page que tous les paramètres de cette URL existent et les afficher: 
 index.php?batiment=12&salle=101  -->

<a href=" index.php?batiment=12&salle=101">Ici</a>
<?php

echo ($_GET["batiment"]);
echo ($_GET["salle"]);

?>










</body>
</html>
